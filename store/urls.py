from django.urls import path

from . import views

app_name = 'store'

urlpatterns = [
    path('', views.home, name='home'),
    path('products/', views.all_products, name='all_products'),
    path('single_product/<slug:product_slug>/', views.product_detail, name='product_detail'),
    path('product_create', views.product_create, name='product_create'),
    path('product_details/<int:pk>', views.product_detail, name='product_details'),
    path('product_update/<int:pk>', views.product_update, name='product_update'),
    path('product_delete/<int:pk>', views.product_delete, name='product_delete'),

    path('category/', views.all_categories, name='all_categories'),
    path('category_list_admin', views.category_list_admin, name="category_list_admin"),
    path('product_list_admin', views.product_list_admin, name="product_list_admin"),
    path('cart_list_admin', views.cart_list_admin, name="cart_list_admin"),
    path('cart_details_admin/<int:pk>', views.cart_details_admin, name="cart_details_admin"),

    path('add_to_cart/<int:product_id>/', views.add_to_cart, name="add_to_cart"),
    path('cart_summary/', views.cart_summary, name="cart_summary"),
    path('cart_success/', views.cart_success, name="cart_success"),

    path('search/<slug:category_slug>/', views.category_list, name='category_list'),
    path('category_details/<slug:category_slug>/', views.category_details, name='category_details'),
    path('category_create', views.category_create, name='category_create'),
    path('category_update/<int:pk>', views.category_update, name='category_update'),
    path('category_delete/<int:pk>', views.category_delete, name="category_delete"),

    path('design_inspiration/', views.design_inspiration, name='design_inspiration'),

    path('furniture/', views.furniture, name='furniture'),
    path('accesories/', views.accesories, name='accesories'),

    path('registration/', views.registration, name='registration'),
    # path('registration/login/', auth_views.LoginView.as_view(template_name='registration/login.html')),
    # path('register', views.user_create_by_form, name='register'),

    # path('searchbox/', views.Searchbox, name='searchbox'),
]
