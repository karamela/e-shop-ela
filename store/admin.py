from django.contrib import admin

from .models import Category, Product, Cart, Cartitem


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = [ 'name', 'price', 'in_stock', 'stock']
    list_filter = ['in_stock', 'is_active']
    list_editable = ['price', 'in_stock']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Cart)
admin.site.register(Cartitem)